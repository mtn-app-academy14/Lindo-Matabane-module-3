import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: HomeRoute(),
    );
  }
}

class HomeRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
        backgroundColor: Colors.red,
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Login Here",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: "Times New Roman",
              fontSize: 25.00,
              fontWeight: FontWeight.w600,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: TextFormField(
                decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Enter your username',
              ),
            ),
          ),
          Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          child: TextFormField(
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Enter your password',
            ),
          ),
        ),
          RaisedButton(
              child: Text('Login'),
              onPressed: () {
                Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => 
                SecondRoute()),);
              },
              padding: EdgeInsets.symmetric(
                horizontal: 70.0,
                vertical: 10.0,
              )),
          RaisedButton(
              child: Text('Registration'),
              onPressed: () {
                Navigator.pushNamed(context, '/third');
              },
              padding: EdgeInsets.symmetric(
                horizontal: 60.0,
                vertical: 10.0,
              )),
        ],
      )),
    );
  }
}

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
        backgroundColor: Colors.green,
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Features",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: "Times New Roman",
              fontSize: 25.00,
              fontWeight: FontWeight.w600,
            ),
          ),
          RaisedButton(
              child: Text('Feature1'),
              onPressed: () {
                Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => 
                FourthRoute()),);
              },
              padding: EdgeInsets.symmetric(
                horizontal: 70.0,
                vertical: 10.0,
              )),
          RaisedButton(
              child: Text('feature2'),
              onPressed: () {
                Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => 
                FifthRoute()),);
              },
              padding: EdgeInsets.symmetric(
                horizontal: 60.0,
                vertical: 10.0,
              )),
              RaisedButton(
              child: Text('profile'),
              onPressed: () {
                Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => 
                SixthRoute()),);
              },
              padding: EdgeInsets.symmetric(
                horizontal: 60.0,
                vertical: 10.0,
              )),
              RaisedButton(
              child: Text('logout'),
              onPressed: () {
                Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => 
                HomeRoute()),);
              },
              padding: EdgeInsets.symmetric(
                horizontal: 50.0,
                vertical: 10.0,
              )),
        ],
      )),
    );
  }
}

class ThirdRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Registration"),
        backgroundColor: Colors.green,
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Register Here",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: "Times New Roman",
              fontSize: 30.00,
              fontWeight: FontWeight.w600,
            ),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: TextField(
            decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Name',
            ),
          ),
        ),
        const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: TextField(
            decoration: InputDecoration(
            border: OutlineInputBorder(),
              hintText: 'Surname',
            ),
          ),
        ),
          RaisedButton(
              child: Text('Register'),
              onPressed: () {
                Navigator.pop(context);
              },
              padding: EdgeInsets.symmetric(
                horizontal: 70.0,
                vertical: 10.0,
              ),
            ),
        ],
        ),
      ),
    );
  }
}

class FourthRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Feature1"),
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "feature1",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: "Times New Roman",
              fontSize: 30.00,
              fontWeight: FontWeight.w600,
            ),
          ),
          RaisedButton(
              child: Text('Dashboard'),
              onPressed: () {
                Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => 
                SecondRoute()),);
              },
              padding: EdgeInsets.symmetric(
                horizontal: 70.0,
                vertical: 10.0,
              ),
            ),
        ],
      ),
      ),
    );
  }
}

class FifthRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("feature2"),
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Feature2",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: "Times New Roman",
              fontSize: 30.00,
              fontWeight: FontWeight.w600,
            ),
          ),
          RaisedButton(
              child: Text('Dashboard'),
              onPressed: () {
                Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => 
                SecondRoute()),);
              },
              padding: EdgeInsets.symmetric(
                horizontal: 70.0,
                vertical: 10.0,
              ),
            ),
        ],
      ),
      ),
    );
  }
}

class SixthRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("User Details Edit"),
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Edit user details",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: "Times New Roman",
              fontSize: 20.00,
              fontWeight: FontWeight.w600,
            ),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: TextField(
            decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Edit name',
            ),
          ),
        ),
        const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: TextField(
            decoration: InputDecoration(
            border: OutlineInputBorder(),
              hintText: 'Edit surname',
            ),
          ),
        ),
          RaisedButton(
              child: Text('Dashboard'),
              onPressed: () {
                Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => 
                SecondRoute()),);
              },
              padding: EdgeInsets.symmetric(
                horizontal: 70.0,
                vertical: 10.0,
              ),
            ),
        ],
      ),
      ),
    );
  }
}